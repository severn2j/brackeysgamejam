﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class Score : MonoBehaviour
{
    bool disableAddScore = false;
    public static int moves;
    public static int score;
    public static int final;
    [SerializeField] Text movesText;
    [SerializeField] Text scoreText;
    [SerializeField] Text finalText;

    private void Awake()
    {
        if (SceneManager.GetActiveScene().buildIndex == 1)
        {
            moves = 0;
            score = 0;
            final = 0;
        }
        else
        {
            movesText.text = moves.ToString();
            scoreText.text = score.ToString();
        }

        if (SceneManager.GetActiveScene().buildIndex == 6)
        {
            disableAddScore = true;
            final = score - moves;
            finalText.text = final.ToString();

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (disableAddScore == false)
        {
            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.DownArrow))
            {
                AddMoveCount();
            }
        }
        
    }

    void AddMoveCount()
    {
        moves++;
        movesText.text = moves.ToString();
        scoreText.text = score.ToString();
    }
}
