﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

 
    
    [SerializeField] float sphereRange = 10f;
    [SerializeField] AudioClip tileClick;
    [SerializeField] AudioClip trapSound;
    [SerializeField] AudioClip winSound;
    

    float gridSpace = 10f;
    bool movementDisabled = false;



    AudioSource audioSource;

    
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();

    }
    void Update ()
    {
        if (!movementDisabled)
        {
            HandleMovement();
        }
        
    }

    private void CheckTile()
    {
        // Check if tile is switched on
        int tileMask = 1 << 10;
        RaycastHit hitTile;
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hitTile, 5, tileMask))
        {
            if (hitTile.collider.GetComponent<MeshRenderer>().enabled == false)
            {
                EnableTilesinSphere();
            }
        }
        // Check for trap
        int trapMask = 1 << 11;
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hitTile, 5, trapMask))
        {
            if (hitTile.collider.CompareTag("Trap"))
            {
                hitTile.collider.GetComponent<MeshRenderer>().enabled = true;
                TurnOffAllTiles();
                Invoke("TurnOffAllTraps", 5f);
                
            }
        }
        // Check for Exit
        int exitMask = 1 << 12;
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hitTile, 5, exitMask))
        {
            if (hitTile.collider.CompareTag("Finish"))
            {
                Win();
                
            }
        }
    }

    private void Win()
    {
        // TODO - Add Win FX and load next level
        movementDisabled = true;
        audioSource.Stop();
        audioSource.PlayOneShot(winSound);
        // Play Win particle FX
        Invoke("LoadNextLevel", 1f);
        
    }

  
    private void LoadNextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    private void TurnOffAllTiles()
    {
        sphereRange = 10f;
        int tileMask = 1 << 10;
        Collider[] hitColliders = Physics.OverlapSphere(transform.localPosition, 160f, tileMask);
        int i = 0;
        int tileCount = 0;
        while (i < hitColliders.Length)
        {
            if (hitColliders[i].GetComponent<MeshRenderer>().enabled == true)
            {
                hitColliders[i].GetComponent<MeshRenderer>().enabled = false;
                tileCount++;
            }

            i++;
        }
        Score.score = Score.score - tileCount;
        audioSource.Stop();
        audioSource.PlayOneShot(trapSound);
        
    }

    private void TurnOffAllTraps()
    {
        int trapMask = 1 << 11;
        Collider[] hitColliders = Physics.OverlapSphere(transform.localPosition, 160f, trapMask);
        int i = 0;
        while (i < hitColliders.Length)
        {
            hitColliders[i].GetComponent<MeshRenderer>().enabled = false;
            i++;
        }
    }

        void EnableTilesinSphere() 
    {
        int tileMask = 1 << 10;
        Collider[] hitColliders = Physics.OverlapSphere(transform.localPosition, sphereRange, tileMask);
        int i = 0;
        int tileCount = 0;
        while (i < hitColliders.Length)
        {
            if (hitColliders[i].GetComponent<MeshRenderer>().enabled == false)
            {
                hitColliders[i].GetComponent<MeshRenderer>().enabled = true;
                tileCount++;
            }
            
            i++;
        }
        Score.score = Score.score + tileCount;
        audioSource.Stop();
        audioSource.PlayOneShot(tileClick);
        sphereRange = sphereRange + 10;
    }

    void HandleMovement()
    {
        
        int wallMask = 1 << 9;

        if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            //RaycastHit hit;
            if (!Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), 10, wallMask))
            {
                transform.position = new Vector3(transform.position.x + gridSpace, transform.position.y, transform.position.z);
                CheckTile();
            }
        }
        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (!Physics.Raycast(transform.position, transform.TransformDirection(Vector3.left), 10, wallMask))
            {
                transform.position = new Vector3(transform.position.x - gridSpace, transform.position.y, transform.position.z);
                CheckTile();
            }
        }
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (!Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), 10, wallMask))
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + gridSpace);
                CheckTile();
            }
        }
        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (!Physics.Raycast(transform.position, transform.TransformDirection(Vector3.back), 10, wallMask))
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - gridSpace);
                CheckTile();
            }
        }
    }

   
}
